#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    // Skriv ut meddelande på skärmen
    cout << " BENSINFORBRUKNING OCH KONSTNAD" << endl;
    cout << " ==============================" << endl << endl;

    // Deklarera mätarställning 1 och 2, hur mycekt bränsle man har tankat,
    // även hur mycket besin kostar per liter
    int fuel_meter_previous, fuel_meter_current;
    float totVolume;
    const float FUEL_PRICE = 8.87;

    // Mata in 3 tal
    cout << " Ange matarstallning 1 [km]:  ";
    cin >> fuel_meter_previous;
    cout << " Ange matarstallning 2 [km]:  ";
    cin >> fuel_meter_current;
    cout << " Hur mycket tankade du [l] :  ";
    cin >> totVolume;

    // Räkna ut bensinförbrukning och milkonstnad
    float bensinforbrukning_literpermil = totVolume / (fuel_meter_current - fuel_meter_previous) * 10;
    float milkostnad = bensinforbrukning_literpermil * FUEL_PRICE;

    // Skriv resultat på skärmen
    cout << fixed << setprecision(2);                 // Skriv ut två decimaler
    cout << " Bensinforbrukning [l/mil] : " << setw(6) << bensinforbrukning_literpermil << endl;
    cout << " Milkostnad        [kr/mil]: " << setw(6) << milkostnad << endl;


    return 0;
}
