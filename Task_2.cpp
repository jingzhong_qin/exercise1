// ---------------------
// Exercise 1, Task_2.cpp
// Program with both logical and syntactical errors
// Erik Ström 2018-11-02
// ---------------------

#include <iostream>                             // vi ska inkludera iostream och namespace för att kunna använda
using namespace std;                            // cout och cin

int main(){
    // Variables and constants
    int radius, circumference, area;
    const float PI = 3.14;

    // Input the circles radius
    cout << "Assign the circle's radius: ";
    cin >> radius;                               // Syntaxfel, man skulle använda " >> " istället för "="

    // Algorithm to caculate circumference (2*PI*r) and area (PI*r*r)
    circumference = PI * radius * radius;
    area = 2 * PI * radius;                      // Syntaxfel, det skulle vara PI istället för pi

    // Output of results
    cout << "A circle with the radius " << radius << " has the circumference";// Syntaxfel, citationstecken får inte
    cout << circumference << " and area " << area << endl;  // skrivas på två rader och man ska använda ;(semikolon)
                                                            // för att avsluta en sats
    // Validate x
    int x;
    cin >> x;                                // Ett meddelande behövs här så att användare vet att man ska mat in något

    if(x == 100)                                  // Syntax fel, det skulle vara " == " istället för "="
        cout << "x is equal to 100" << endl;
    if(x > 0);                                    // Logisk fel, 100 är också större än 0
        cout << "x is larger than zero" << endl;

    switch(x) {
        case 5 :
            cout << "x is equal to 5 " << endl;
        case 10 :
            cout << "x is equal to 10" << endl;
        default :
            cout << "x is neither 5 nor 10" << endl;
              }                                    // Syntax fel, det saknar en  klammerparentes.
     return 0;
    } // End main

